
echo off

cls

@rem setup the config params
set SETTINGSFILE=%~dp0%Params.val
set ListOfBases=%~dp0%ListOfBases.val
set ScriptDir=%~dp0%
set TestScript=test

if /I %TestScript%==test (
	echo %ScriptDir%
	echo %SETTINGSFILE%
	echo %BackupCMDFileName%
)

for /F "eol=# delims== tokens=1,2" %%i in (%SETTINGSFILE%) do (
    @rem В переменной i - ключ
    @rem В переменной j - значение
    @rem Транслируем это в переменные окружения
    set %%i=%%j
	if /I %TestScript%==test (
		echo %%i=%%j
	)
)

set BackupCMDFileName=%ScriptDir%%ScriptBackup_1C_base%

@rem goto aend

for /F "eol=# delims== tokens=1,2" %%i in (%ListOfBases%) do (
    @rem В переменной i - ключ
    @rem В переменной j - значение
	if /I %TestScript%==test (
		echo %BackupCMDFileName% %%i %%j
	)
    call %BackupCMDFileName% %%i %%j
)

:aend