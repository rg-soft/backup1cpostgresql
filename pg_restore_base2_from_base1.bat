@rem rutine sets

@rem { Обязательно заполнить и проверить!

SET ar_base_from=ut_work
SET ar_base_to=ut_test1
SET ar_date_time_from=2019_10_01-Q-02_02
SET ar_path_arc=C:\PG\Backup\ut
@rem завершать слэшом НЕ надо! Пример: ar_path_arc=C:\PG\Backup\buh 

@rem пример пути к каталогам архива:
@rem исходный архив: C:\PG\Backup\ut\ut_work-date-2019_07_04-D-02_02.7z
@rem полученный путь для архива: C:\PG\Backup\ut\ut_work-date-2019_07_04-D-02_02\

@rem } Обязательно заполнить и проверить!

@rem PG sets
SET ar_host=localhost
SET ar_port=5432
SET ar_user=postgres
SET ar_jobs=4
SET ar_path_bin=D:\PostgresPro1C\9.6\bin
@rem path sets
SET ar_delimiter=-date-
SET ar_BackupDirectory=%ar_path_arc%\%ar_base_from%%ar_delimiter%%ar_date_time_from%
SET ar_PGBackupDirectory=%ar_path_arc:\=\\%\\%ar_base_from%%ar_delimiter%%ar_date_time_from%

SET ar_begin=%date%  %time%  begin

@rem ”дал¤ем базу %ar_base_to%, если она существует без подтверждени¤ об удалении. ѕеред удалением закрываем все активные соединени¤ с базой, кроме текущего
"%ar_path_bin%\psql.exe" --host "%ar_host%" --port %ar_port% --username "%ar_user%" --dbname="%ar_base_to%" --no-password --command "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '%ar_base_to%' AND pid <> pg_backend_pid();"
"%ar_path_bin%\dropdb.exe" --host="%ar_host%" --port=%ar_port% --username="%ar_user%" --if-exists --no-password --echo "%ar_base_to%" 

@rem —оздаем заново базу %ar_base_to%, если ее не существует
"%ar_path_bin%\createdb.exe" --host="%ar_host%" --port=%ar_port% --username "%ar_user%" --no-password --echo --owner="%ar_user%" --encoding="UTF8" --locale="Russian_Russia.1251" --tablespace="pg_default" "%ar_base_to%"

@rem ¬осстанавливаем данные всех схем и таблиц в базу %ar_base_to% из архива базы %ar_base_from%, который не содержит данных таблицы config (только ее схему)
"%ar_path_bin%\pg_restore.exe" --host "%ar_host%" --port %ar_port% --username "%ar_user%" --role "%ar_user%" --dbname "%ar_base_to%" --no-password --jobs=%ar_jobs% --verbose "%ar_BackupDirectory%\TABLES.backup"

@rem ¬осстанавливаем данные таблицы config в базу %ar_base_to% из архива базы %ar_base_from%
"%ar_path_bin%\psql.exe" --host "%ar_host%" --port %ar_port% --username "%ar_user%" --dbname="%ar_base_to%" --no-password --command "\COPY public.config FROM '%ar_PGBackupDirectory%\\CONFIG.backup\\config' WITH BINARY;"

@rem ¬ыводим врем¤ начала и конца восстановлени¤ базы

@echo %ar_begin%
@echo %date%  %time%  end