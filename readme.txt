Срипты бэкапирования баз 1С.
Params.val - файл настроек для автоматической работы
ListOfBases.val - Список баз к архивированию
Backup_all_bases.cmd - запускаемый регламентом или вручную файл. Файл читает настройки из Params.val
Backup_1C_base_v2.cmd - запускается файлом Backup_all_bases.cmd для каждой базы
--===--
pg_backup_base.bat - бэкапирование в ручном режиме
pg_restore_base2_from_base1.bat - восстановление из бэкапа