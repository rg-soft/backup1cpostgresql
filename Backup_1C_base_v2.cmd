
echo off
echo =Start script==============================================================
rem %3 - not used
echo Params in call: %1 %2 %3

rem goto pend

rem { Setup date in file name
rem set DateInName=%date:~6,4%_%date:~3,2%_%date:~0,2%-%time:~0,2%_%time:~3,2%
set nyear=%date:~6,4%
set nmonth=%date:~3,2%
set nmonth=%nmonth: =0%
set nday=%date:~0,2%
set nday=%nday: =0%
set nhour=%time:~0,2%
set nhour=%nhour: =0%
set nmin=%time:~3,2%
set nmin=%nmin: =0%

echo nmonth=%nmonth%
echo nday=%nday%
echo nhour=%nhour%
echo nmin=%nmin%

set part_of_name=""

if %nmonth% equ 04 (
	set part_of_name=Q
) else (
	if %nmonth% equ 07 (
		set part_of_name=Q
	) else (
		if %nmonth% equ 10 (
			set part_of_name=Q
		) else (
			if %nmonth% equ 01 (
				set part_of_name=Q
			)
		)
	)
)

if %nday% neq 01 (
	set part_of_name=D
) else (
	if %part_of_name% neq Q (
		set part_of_name=M
	)
)

echo part_of_name=%part_of_name%

rem goto pend

set DateInName=%nyear%_%nmonth%_%nday%-%part_of_name%-%nhour%_%nmin%
rem } Setup date in file name 

rem { Setup control vars
set BaseName=%1

rem Setup directory and file names
set BackupFileSQLName=%WorkDirectory%\%BaseName%%DelimiterND%%DateInName%
set PGBackupFileSQLName=%BackupFileSQLName:\=\\%
set BackupFileZipName=%WorkDirectory%\%BaseName%%DelimiterND%%DateInName%.7z
set BackupDirectory=%MainBackupDirectory%\%2
set CheckZipName=%BackupDirectory%\%BaseName%%DelimiterND%%DateInName%.7z
set FPMaskOldDayZipFiles="%BackupDirectory%\%BaseName%*-D-*.7z"
set MaskOldZipFiles="%WorkDirectory%\%BaseName%%DelimiterND%*.7z"

@rem set cmd_pg_dump="%AppDir%\pg_dump.exe" --host=localhost --port=5432 --username %DBUser1c% --format=custom --file=%BackupFileSQLName% %BaseName%
@rem Создаем архив схем и данных всех таблиц базы %ar_base%, кроме данных таблицы config (для config - только схема)
set cmd_pg_dump="%AppDir%\pg_dump.exe" --host "%DBHost%" --port %DBPort% --username "%DBUser1c%" --role "%DBUser1c%" --no-password --format directory --jobs=%DBJobs% --blobs --encoding UTF8 --verbose --exclude-table-data=config --file "%BackupFileSQLName%\TABLES.backup" "%BaseName%"
set cmd_psql="%AppDir%\psql.exe" --host "%DBHost%" --port %DBPort% --username "%DBUser1c%" --no-password --command "COPY public.config TO '%PGBackupFileSQLName%\\CONFIG.backup\\config' WITH BINARY;" --dbname="%BaseName%"

set cmd_pack=%ArcMaker% %BackupZipKeys% %BackupZipSwitches% "%BackupFileZipName%" %BackupFileSQLName%
set cmd_copy=%CmdCopyFiles% "%BackupFileZipName%" %BackupDirectory%\
rem } Setup control vars

if exist "%BackupFileSQLName%" (
	rd /s /q "%BackupFileSQLName%" 
	echo.
	echo Archive directory exist. Deleted "%BackupFileSQLName%"
)

@rem Создаем каталог архива с данными таблиц и config базы %BaseName%
md "%BackupFileSQLName%"

@rem Создаем архив с данными таблицы config базы %BaseName%
md "%BackupFileSQLName%\CONFIG.backup"

@rem goto pend

rem { Check main vars
if /I %TestScript%==test (
	echo.
	echo Date in filename       is %DateInName%
	echo Archiver               is %ArcMaker%
	echo Working directory      is %WorkDirectory%
	echo Backup file SQL name   is %BackupFileSQLName%
	echo Backup file SQL PGname is %PGBackupFileSQLName%
	echo Backup file Zip name   is %BackupFileZipName%
	echo Backup directory       is %BackupDirectory%
	echo Checking file Zip name is %CheckZipName%
	echo Mask of old zip files  is %MaskOldZipFiles%
	echo Mask of old Dzip files is %MaskOldDayZipFiles%
	echo cmd_pg_dump            is %cmd_pg_dump%
	echo cmd_pack               is %cmd_pack%
	echo cmd_copy               is %cmd_copy%
	echo TestScript             is %TestScript%
	echo MaskOldDayZipFiles     is %MaskOldDayZipFiles%
	echo FPMaskOldDayZipFiles   is %FPMaskOldDayZipFiles%
	echo MaxOldDaysFiles        is %MaxOldDaysFiles%
)
rem } Check main vars

rem { Before will be archiving copy old zip files

@rem goto pend

echo.
for %%B in (%MaskOldZipFiles%) do (
	
	if /I %TestScript%==test (
		echo Command for copy Zip is %CmdCopyFiles% "%%B" "%BackupDirectory%\"
		echo File name Zip finaly is "%BackupDirectory%\%%~nB.7z"
	)
	
	%CmdCopyFiles% "%%B" "%BackupDirectory%\"

	if exist "%BackupDirectory%\%%~nB.7z" (
		del "%%B"
		echo Deleted 7z tempfile "%%B"
	) else (
		echo Backuped file %BackupFileSQLName% in %BackupDirectory% not exist!
	)
)
rem } Before will be archiving copy old zip files

@rem goto pend

@rem echo on
@REM echo %cmd_pg_dump% 
@REM echo %cmd_psql% 
@REM echo %cmd_pack%
@REM echo %cmd_copy%

%cmd_pg_dump%
%cmd_psql%
%cmd_pack% && %cmd_copy%
@rem echo off

@rem goto pend

@rem { Prepare message for e-mail send
echo.
if exist "%CheckZipName%" (
	set ZipFinaly=1
	set TextOfCheckedTheFile=Archived of the base '%BaseName%' packed into '%CheckZipName%' and checked
	
	rd /s /q "%BackupFileSQLName%" 
	echo Deleted SQL tempfile %BackupFileSQLName%
	
) else (
	set ZipFinaly=0
	set TextOfCheckedTheFile=Archive of the base '%BaseName%' NOT copied correctly into '%CheckZipName%'
)
if /I %TestScript%==test (
	echo %TextOfCheckedTheFile%
)

if exist "%BackupFileZipName%" (
	set ZipWork=1
	set TextOfCheckedTheZipFile=Zip file '%BackupFileZipName%' created
) else (
	set ZipWork=0
	set TextOfCheckedTheZipFile=Zip file '%BackupFileZipName%' NOT created
)
if /I %TestScript%==test (
	echo %TextOfCheckedTheZipFile%
)

@REM if exist %BackupFileSQLName% (
	@REM set SQLWork=1
	@REM set TextOfCheckedTheSQLFile="SQL file %BackupFileSQLName% created."
@REM ) else (
	@REM set SQLWork=0
	@REM set TextOfCheckedTheSQLFile="SQL file %BackupFileSQLName% NOT created."
@REM )

if /I %TestScript%==test (
	echo %TextOfCheckedTheSQLFile%
)

@rem set LogText="%TextOfCheckedTheFile%. %TextOfCheckedTheZipFile%. %TextOfCheckedTheSQLFile%."
set LogText="%TextOfCheckedTheFile%. %TextOfCheckedTheZipFile%."
if /I %TestScript%==test (
	echo.
	echo %LogText%

	echo Backup_Mail_Send.vbs %BaseName% "%BackupFileZipName%" %BackupDirectory% %LogText% %EmailFrom% %EmailTo% %smtpserver% %smtpserverport%
)

if /I %EnableEMail%==Yes (
	call Backup_Mail_Send.vbs %BaseName% "%BackupFileZipName%" %BackupDirectory% %LogText% %EmailFrom% %EmailTo% %smtpserver% %smtpserverport%
)
@rem } Prepare message for e-mail send

@rem { Delete files older then N days

@rem debug :pend
@rem debug set MaxOldDaysFiles=3
set count_files=0

Setlocal EnableDelayedExpansion

echo.
For /F "delims=" %%A In ('Dir "%FPMaskOldDayZipFiles%" /O-N /B /A-D') Do (

	set /a count_files=!count_files!+1
	if !count_files! GTR %MaxOldDaysFiles% (
		echo delete file "%BackupDirectory%\%%A"
		%CmdDeleteFile% "%BackupDirectory%\%%A"
	)
)

echo.
echo Daily files in %BackupDirectory% is %count_files%

Endlocal
@rem } Delete files older then N days

@rem Завершение работы
:pend
echo =End script================================================================
