SET ar_host=localhost
SET ar_port=5432
SET ar_user=postgres
SET ar_jobs=8
SET ar_base=delete
SET ar_date=%date:.=_%
SET ar_time=%time:~0,-3%
SET ar_time=%ar_time::=_%
SET ar_time=%ar_time: =0%
SET ar_path_bin=E:\PostgreSQL1C\11\bin
SET ar_path_arc=c:\PGWorks
SET ar_delimiter=-date-
SET ar_base_date_time=%ar_base%%ar_delimiter%%ar_date%-%ar_time%
SET ar_BackupDirectory=%ar_path_arc%\%ar_base_date_time%
SET ar_PGBackupDirectory=%ar_path_arc:\=\\%\\%ar_base_date_time%

if exist "%ar_BackupDirectory%" (
	rd /s /q "%ar_BackupDirectory%" 
	echo.
	echo Archive directory exist. Deleted "%BackupDirectory%"
)

md "%ar_BackupDirectory%"

@rem —оздаем архив схем и данных всех таблиц базы %ar_base%, кроме данных таблицы config (дл¤ config - только схема)
"%ar_path_bin%\pg_dump.exe" --host "%ar_host%" --port %ar_port% --username "%ar_user%" --role "%ar_user%" --no-password --format directory --jobs=%ar_jobs% --blobs --encoding UTF8 --verbose --exclude-table-data=config --file "%ar_BackupDirectory%\TABLES.backup" "%ar_base%"

@rem —оздаем архив с данными таблицы config базы %ar_base%
md "%ar_BackupDirectory%\CONFIG.backup"
"%ar_path_bin%\psql.exe" --host "%ar_host%" --port %ar_port% --username "%ar_user%" --no-password --command "COPY public.config TO '%ar_PGBackupDirectory%\\CONFIG.backup\\config' WITH BINARY;" --dbname="%ar_base%"
